 const cacheName='pwa-cache-1.0';
 var urlsToCache = [ 'index.html','js/main.js'];

 self.addEventListener('install', function(event) {
     event.waitUntil(caches.open(cacheName)
         .then(function(cache) {
             console.log('Opened cache');
             return cache.addAll(urlsToCache);
         }));
 });

 self.addEventListener('activate', function(event) {
     console.log('activated', event);
 });

 self.addEventListener('fetch', evt => {
     evt.respondWith(
     caches.match(evt.request).then(res => {
         if (res) {
             console.log("From cache");
             return res;
         }
         return fetch(evt.request).then(newResponse => {
             console.log("From network");
             caches.open(cacheName).then(cache => cache.put(evt.request, newResponse));
             return newResponse.clone();
         });
     }
     ));
 });


